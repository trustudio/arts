package com.trustudio.arts.v2.http;

import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.trustudio.arts.v2.model.JenisDokumen;
import com.trustudio.arts.v2.model.SiteCode;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.StringUtil;

public class ArtsConnection extends RestConnection{
	public ArrayList<JenisDokumen> getJenisDokumen() throws Exception{
		ArrayList<JenisDokumen> list = null;
		
		try {
			String url 		= Cons.API_URL_JenisDokumen;
			
			InputStream is	= connectGet(url);
			
			if (is != null) {
				String response		= StringUtil.streamToString(is);
				JSONObject jsonObj 	= (JSONObject) new JSONTokener(response).nextValue(); 
				
				Debug.i(response);
				
				if (jsonObj.getString("status").equals("ok")) {
					JSONArray jsonArr = jsonObj.getJSONArray("data");
					
					int length = jsonArr.length();
					
					if (length > 0) {
						list = new ArrayList<JenisDokumen>();
						
						for (int i = 0; i < length; i++) {
							JSONObject jsonUser = jsonArr.getJSONObject(i);
							
							JenisDokumen user = new JenisDokumen();
							
							user.Id		= jsonUser.getString("Id");
							user.Name	= jsonUser.getString("Name");
							
							list.add(user);
						}
					}
				}
		        
				is.close();
			}	
		} catch (Exception e) {
			throw e;
		} 
		
		
		return list;
	}
	
	public ArrayList<SiteCode> getSite() throws Exception{
		ArrayList<SiteCode> list = null;
		
		try {
			String url 		= Cons.API_URL_site;
			
			InputStream is	= connectGet(url);
			
			if (is != null) {
				String response		= StringUtil.streamToString(is);
				JSONObject jsonObj 	= (JSONObject) new JSONTokener(response).nextValue(); 
				
				Debug.i(response);
				
				if (jsonObj.getString("status").equals("ok")) {
					JSONArray jsonArr = jsonObj.getJSONArray("data");
					
					int length = jsonArr.length();
					
					if (length > 0) {
						list = new ArrayList<SiteCode>();
						
						for (int i = 0; i < length; i++) {
							JSONObject jsonUser = jsonArr.getJSONObject(i);
							
							SiteCode user = new SiteCode();
							
							user.siteid	= jsonUser.getString("SiteCode");
							
							list.add(user);
						}
					}
				}
		        
				is.close();
			}	
		} catch (Exception e) {
			throw e;
		} 
		
		
		return list;
	}
}
