package com.trustudio.arts.v2.http;

import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import org.apache.http.impl.client.DefaultHttpClient;

import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;

/**
 * Base http connection for GET and POST.
 * 
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 *
 */
public class RestConnection {		
	public InputStream connectGet(String url) throws Exception {
		HttpClient httpClient 	= new DefaultHttpClient();		
		HttpGet httpGet 		= new HttpGet(url); 
		InputStream stream		= null;
		
		Debug.i(Cons.TAG, "GET " + url);
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			if (httpEntity == null) throw new Exception();
			
			stream	= httpEntity.getContent();			
		} catch (Exception e) { 
			throw e;
		}
		
		return stream;
	}
	
	public InputStream connectPost(String url, List<NameValuePair> params) throws Exception {
		HttpClient httpClient 	= new DefaultHttpClient();
		HttpPost httpPost 		= new HttpPost(url); 
		InputStream stream		= null;
		
		Debug.i(Cons.TAG, "POST " + url);
		
		try {
	        httpPost.setEntity(new UrlEncodedFormEntity(params));
	        
	        HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			if (httpEntity == null) throw new Exception("");
			
			stream = httpEntity.getContent();			
		} catch (Exception e) {
			throw e;
		}
		
		return stream;
	}
	
	public InputStream connectPut(String url, List<NameValuePair> params) throws Exception {
		HttpClient httpClient 	= new DefaultHttpClient();
		HttpPut httpPut 		= new HttpPut(url); 
		InputStream stream		= null;
		
		Debug.i(Cons.TAG, "POST " + url);
		
		try {
			httpPut.setEntity(new UrlEncodedFormEntity(params));
	        
			HttpResponse httpResponse = httpClient.execute(httpPut);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			if (httpEntity == null) throw new Exception("");
			
			stream = httpEntity.getContent();			
		} catch (Exception e) {
			throw e;
		}
		
		return stream;
	}
	
	public InputStream connectPost(String url) throws Exception {
		HttpClient httpClient 	= new DefaultHttpClient();
		HttpPost httpPost 		= new HttpPost(url); 
		InputStream stream		= null;
		
		Debug.i(Cons.TAG, "POST " + url);
		
		try {
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			if (httpEntity == null) throw new Exception("");
			
			stream = httpEntity.getContent();			
		} catch (Exception e) {
			throw e;
		}
		
		return stream;
	}
}