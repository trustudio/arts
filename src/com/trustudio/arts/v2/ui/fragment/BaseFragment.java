package com.trustudio.arts.v2.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.trustudio.arts.v2.ui.BaseActivity;

public class BaseFragment extends SherlockFragment {
	
	public SQLiteDatabase getDatabase() {		
		return ((BaseActivity) getActivity()).getDatabase();
	}
	
	public SharedPreferences getSharedPreferences() {
		return ((BaseActivity) getActivity()).getSharedPreferences();
	}
	
	public void showInfo(String title, String info) {
		((BaseActivity) getActivity()).showInfo(title, info);
	}
	
	public void showInfo(String info) {
		((BaseActivity) getActivity()).showInfo(info);
	}
	
	public void showError(String error) {
		((BaseActivity) getActivity()).showError(error);
	}
	
	public void showSuccess(String message) {
		((BaseActivity) getActivity()).showSuccess(message);
	}
	
	public void showSuccess(String message, boolean back) {
		((BaseActivity) getActivity()).showSuccess(message, back);
	}
	
	public String getActivityTag() {
		return ((BaseActivity) getActivity()).getTag();
	}
	
	public void showToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}
	
	public void showToast(int message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}
	
	public void progressIntermediate(boolean status){
		((BaseActivity) getActivity()).setSupportProgressBarIndeterminateVisibility(status);
	}
	
	public void showDialogMessage(String title, String message){
		((BaseActivity) getActivity()).showDialogMessage(title, message);
	}
	
	public Context getThemedContext(){
		return ((BaseActivity) getActivity()).getSupportActionBar().getThemedContext();
	}
	
	public ActionBar getSupportActionBar(){
		return ((BaseActivity) getActivity()).getSupportActionBar();
	}
}
