package com.trustudio.arts.v2.ui.fragment;

import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.http.ArtsConnection;
import com.trustudio.arts.v2.model.JenisDokumen;
import com.trustudio.arts.v2.ui.adapter.DocumentListAdapter;

public class DocumentFragment extends BaseFragment{
	private Context context;
	private DocumentListAdapter mAdapter;
	
	private OnDocumentListListener mListener;
	

	private boolean mDurationLoading = false;
	private ProgressDialog mProgressDialogDuration;
	private ArtsConnection conn;
	private ArrayList<JenisDokumen> mList;
	private ListView mListView;
	private JenisDocumentTask mTask;
	
	public static DocumentFragment newInstance() {
		return new DocumentFragment();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context 	= activity;
		mListener	= (OnDocumentListListener) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_document, null);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mListView 	= (ListView) getView().findViewById(R.id.list_document);
		
		conn				= new ArtsConnection();
		mProgressDialogDuration = new ProgressDialog(context);
		mAdapter			= new DocumentListAdapter(context);
		
		//mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mListener.onDocumentClickListener(mList.get(arg2).Name); 
			}
		});
		
		(mTask = new JenisDocumentTask()).execute();
	}
	
	public interface OnDocumentListListener{
		void onDocumentClickListener(String jenis_dokumen);
	}
	
	@Override
	public void onPause() {
		if(mDurationLoading && mTask != null){
			mTask.cancel(true);
		}
		
		super.onPause();
	}
	
	private class JenisDocumentTask extends AsyncTask<URL, Integer, Long>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			mDurationLoading = true;
			progressIntermediate(true);
			mProgressDialogDuration.setMessage("Meminta Jenis Dokumen....");
			mProgressDialogDuration.show();
			mProgressDialogDuration.setCanceledOnTouchOutside(false);
		}
		
		@Override
		protected Long doInBackground(URL... arg0) {
			long result = 0;
			
			try {
				mList = conn.getJenisDokumen();
				result = 1;
			} catch (Exception e) {
				mDurationLoading = false;
				e.printStackTrace();
			}
			
			return result;
		}
		
		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
			mDurationLoading = false;
			progressIntermediate(false);
			mProgressDialogDuration.dismiss();
			
			if(result == 1){
				mAdapter.setData(mList);
				mListView.setAdapter(mAdapter);
			}else{
				showToast("Request data gagal...");
			}
		}
	}
}
