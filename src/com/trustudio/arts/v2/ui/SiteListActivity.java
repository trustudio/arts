package com.trustudio.arts.v2.ui;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.json.JSONObject;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.http.AsyncHttpResponseHandler;
import com.trustudio.arts.v2.http.NexwaveClient;
import com.trustudio.arts.v2.loader.TemplateLoader;
import com.trustudio.arts.v2.loader.TemplateLoader.OnTemplateLoaderListener;
import com.trustudio.arts.v2.model.ConfigModel;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.OpenData;
import com.trustudio.arts.v2.util.QuestionCons;
import com.trustudio.arts.v2.util.StorageManager;
import com.trustudio.arts.v2.util.URLCons;
import com.trustudio.arts.v2.util.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SiteListActivity extends BaseActivity{
	public final static String DIR = "DIR";


	private String provider = "TRI";
	//private String pekerjaan = "Existing 2G %26 3G";
	private String dokumen;
	private String siteID;
	private String link;
	

	private String[] mItem 	= { "Nominal", "Tracking", "Candidate" };
	private String[] mItem2 = { "Candidate" };
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_upload);
		
		Bundle bundle = getIntent().getExtras();
		siteID = bundle.getString("com.site");
		dokumen = bundle.getString("com.jenis_dokumen");
		
		Debug.i(siteID);
		Debug.i(dokumen);
		Debug.i(provider);
		
		SiteListAdapter mAdapter = new SiteListAdapter(getApplicationContext());
		
		final String folder;
		try {
			String provider = URLDecoder.decode(this.provider, URLCons.UTF_8);
			//String pekerjaan = URLDecoder.decode(this.pekerjaan, URLCons.UTF_8);
			String dokumen = URLDecoder.decode(this.dokumen, URLCons.UTF_8);
			String siteCode = URLDecoder.decode(siteID, URLCons.UTF_8);
			
			folder = StorageManager.createPath(new String[] { provider, dokumen, siteCode });
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		ListView list = (ListView) findViewById(R.id.list_image);
		
		if(dokumen.equals("SSR")){
			mAdapter.setData(mItem);
		}else{
			mAdapter.setData(mItem2);
		}
		
		list.setAdapter(mAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int pos,
					long id) {
				switch (pos) {
				case 0:
					if(dokumen.equals("SSR")){
						Intent intent = new Intent(getApplicationContext(), CandidateLocationActivity.class);
						
						intent.putExtra("com.siteid", siteID);
						intent.putExtra("com.provider", provider);
						intent.putExtra("com.dokumen", dokumen);
						
						startActivity(intent);
					}else{
						try {
							submit();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					break;
				case 2:
					try {
						submit();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 1:
					Intent intent1 = new Intent(SiteListActivity.this, TrackingActivity.class);
	            	
	            	intent1.putExtra("com.siteid", siteID);
					intent1.putExtra("com.provider", provider);
					intent1.putExtra("com.dokumen", dokumen);
	            	
	            	startActivity(intent1);
					break;
				}
			}
		});
	}
	
	public void submit() throws UnsupportedEncodingException {

		siteID 		= URLEncoder.encode(siteID, URLCons.UTF_8).trim();
		//pekerjaan 	= URLEncoder.encode(pekerjaan, URLCons.UTF_8).trim();
		dokumen 	= URLEncoder.encode(dokumen, URLCons.UTF_8).trim();
		provider 	= URLEncoder.encode(provider, URLCons.UTF_8).trim();
		
		//* Edited sigue *//
		link = String.format(URLCons.GET_TEMPLATE, siteID, dokumen, provider)
				.replace(" ", "");
		Log.d("link for templateid", link);
		int templateID = OpenData.getTemplateID(getApplication(), link);
		
		Debug.i("template id = " + String.valueOf(templateID));
		
		String sitecode = OpenData.getSiteCodeData(getApplication(), provider, dokumen);
		
		/* If not connected */
		if(!Utils.isInternetOn(getApplication())){
			if(sitecode != null){
				link = String.format(URLCons.GET_TEMPLATE, sitecode, dokumen, provider)
					.replace(" ", "");
				Log.d("link template offline", link);
				
				templateID = OpenData.getTemplateID(getApplication(), link);
			}
		}
		
		/* if template available */
		if (templateID != -1) {
			String templateData = OpenData.getTemplate(getApplication(), templateID);

			/* if question data is available */
			if (!OpenData.isQuestionDataEmpty(getApplication(), templateID)) {
				digestData(link, templateData);
				return;
			}

			/* download question data */
			if (templateData != null)
				digestData(link, templateData);
			return;
		}

		final ProgressDialog pd = new ProgressDialog(SiteListActivity.this);
		pd.setMessage("Downloading data...");
		pd.show();

		Log.d("Template Link", link); // TODO

		NexwaveClient.get(SiteListActivity.this, link, null, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				Log.d("getTemplateResponse", content);
				try {

					JSONObject json = new JSONObject(content);
					String status = json.getString(NexwaveClient.STATUS);

					Log.i("status", status);

					if (status.trim().equalsIgnoreCase(NexwaveClient.STATUS_OK)) {
						digestData(link, content);
					}

				} catch (Exception e) {
					onFailure(e, e.toString());
				}

			}

			@Override
			public void onFailure(Throwable error, String content) {
				Log.e("getTemplate", error.toString());
				NexwaveClient.cancel(SiteListActivity.this);
				onShowError();
			}

			@Override
			public void onFinish() {
				pd.dismiss();
			}

		});
	}	

	private void onShowError() {
		Toast.makeText(getApplicationContext(), "Data not found or invalid", Toast.LENGTH_LONG).show();
	}
	
	private void digestData(String iden, String templateData) {
		Debug.i("Diggest Data");
		
		try {
			String provider = URLDecoder.decode(this.provider, URLCons.UTF_8);
			//String pekerjaan = URLDecoder.decode(this.pekerjaan, URLCons.UTF_8);
			String dokumen = URLDecoder.decode(this.dokumen, URLCons.UTF_8);
			String siteCode = URLDecoder.decode(this.siteID, URLCons.UTF_8);

			final String folder = StorageManager.createPath(new String[] { provider, dokumen,
					siteCode });

			Log.i("Folder", folder);
			
			TemplateLoader loader = new TemplateLoader(this);
			loader.loadData(iden, siteCode, provider, dokumen, templateData, new OnTemplateLoaderListener() {

				@Override
				public void onLoadComplete(int templateDocID) {
					Intent intent = new Intent(SiteListActivity.this, FormViewerActivity.class);

					Log.i("tempalteDocId", String.valueOf(templateDocID));
					
					intent.putExtra(ConfigModel.PROVIDER, SiteListActivity.this.provider);
					intent.putExtra(ConfigModel.DOKUMEN, SiteListActivity.this.dokumen);
					//intent.putExtra(ConfigModel.JOB, SiteListActivity.this.pekerjaan);
					intent.putExtra(ConfigModel.SITE_CODE, SiteListActivity.this.siteID);
					
					intent.putExtra(QuestionCons.TEMPLATE_DOC_ID, templateDocID);
					intent.putExtra(FormViewerActivity.DIR, folder);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					startActivity(intent);

					finish();
				}
			});
			
			Debug.i("Diggest Success");
		} catch (Exception e) {
			e.printStackTrace();
			
			Debug.i("Diggest Failed");
		}
	}
	
	public class SiteListAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		private String[] mItem;
		
		public SiteListAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}
		
		public void setData(String[] mItem){
			this.mItem = mItem;
		}
		
		@Override
		public int getCount() {
			return mItem.length;
		}

		@Override
		public Object getItem(int position) {
			return mItem[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			
			if (convertView == null) {
				convertView	=  mInflater.inflate(android.R.layout.simple_list_item_1, null);
				
				holder = new ViewHolder();
				
				holder.mTextTitle		= (TextView) convertView.findViewById(android.R.id.text1);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.mTextTitle.setTextColor(R.color.text_grey);
			holder.mTextTitle.setText(mItem[position]);
			
	        return convertView;
		}
		
		class ViewHolder{
			TextView mTextTitle;
		}
	}

}
