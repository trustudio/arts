package com.trustudio.arts.v2.ui;

import java.util.ArrayList;

import com.actionbarsherlock.view.MenuItem;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.model.JenisDokumen;
import com.trustudio.arts.v2.ui.fragment.DocumentFragment;
import com.trustudio.arts.v2.ui.fragment.DocumentFragment.OnDocumentListListener;
import com.trustudio.arts.v2.ui.fragment.ProviderFragment;
import com.trustudio.arts.v2.ui.fragment.ProviderFragment.OnProviderListListener;
import com.trustudio.arts.v2.ui.fragment.SiteFragment;
import com.trustudio.arts.v2.ui.fragment.SiteFragment.OnSiteListener;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ConfigFormActivity extends BaseActivity implements OnProviderListListener, OnDocumentListListener, OnSiteListener{
	private SiteFragment mFragmentSite;
	private ProviderFragment mFragmentProvider;
	private DocumentFragment mFragmentDocument;
	
	private int frag_position = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form_config);
		
		getSupportActionBar().setTitle(getTitles(0));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);

		if(savedInstanceState == null){
			mFragmentProvider = ProviderFragment.newInstance();
	
			getSupportFragmentManager().beginTransaction().add(R.id.fragment, mFragmentProvider).commit();
		}
	}

	@Override
	public void onBackPressed() {
		if(frag_position > 0){
			setTitles(getTitles(--frag_position));
			super.onBackPressed();
		}else if(frag_position == 0){
			confirmExit();
		}
	}
	
	private void setTitleForward() {
		if (frag_position == lengthTitles())
			return;
		
		frag_position++;
		setTitles(getTitles(frag_position));
	}
	
	@Override
	public void onDocumentClickListener(String jenis_dokumen) {
		setTitleForward();
		
		mFragmentSite			= SiteFragment.newInstance(jenis_dokumen);
		FragmentTransaction ft 	= getSupportFragmentManager().beginTransaction();
		ft.addToBackStack(null);
		ft.replace(R.id.fragment, mFragmentSite).commit();
	}

	@Override
	public void onProviderClickListener() {
		setTitleForward();
		
		mFragmentDocument 		= DocumentFragment.newInstance();
		FragmentTransaction ft 	= getSupportFragmentManager().beginTransaction();
		ft.addToBackStack(null);
		ft.replace(R.id.fragment, mFragmentDocument).commit();
	}
	
	@Override
	public void onSubmitCLickLIstener() {
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
}
