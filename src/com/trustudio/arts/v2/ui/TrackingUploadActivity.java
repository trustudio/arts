package com.trustudio.arts.v2.ui;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.db.MasterDb;
import com.trustudio.arts.v2.http.UploadRest;
import com.trustudio.arts.v2.model.TrackingModel;
import com.trustudio.arts.v2.ui.adapter.TrackingUploadAdapter;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.ImageUploader;
import com.trustudio.arts.v2.util.ImageUploader.ImageUploadListener;

public class TrackingUploadActivity extends BaseActivity{
	private ProgressDialog mProgress;
	private UploadRest mRest;
	private String path ;
	
	private String provider;
	private String dokumen;
	private String siteid;
	
	private MasterDb mMasterDb;
	private ArrayList<TrackingModel> mList;
	private TrackingUploadAdapter mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_upload);
		
		getSupportActionBar().setTitle("Upload Photo");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);
		
		Bundle bundle = getIntent().getExtras();
		provider= bundle.getString("com.provider");
		dokumen	= bundle.getString("com.dokumen");
		siteid	= bundle.getString("com.siteid");
		
		ListView  lv = (ListView)findViewById(R.id.list_image);
		
		mRest		= new UploadRest();
		mMasterDb	= new MasterDb(mSqLite);
		mProgress 	= new ProgressDialog(TrackingUploadActivity.this);
		mAdapter	= new TrackingUploadAdapter(getApplicationContext());
		
	    mList		= mMasterDb.getTrackingList(provider, dokumen, siteid); 
	    
	    mAdapter.setData(mList);
	    
	    lv.setAdapter(mAdapter);
	    
	    lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				path = null;
				
				try {
					path = mList.get(position).path;
					
					if(path != null)
						doPost(mList.get(position).no_tracking, mList.get(position).caption);
					else
						showToast("Path null!");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	private void doPost(final String var, final String caption) {
		mProgress.setMessage("Uploading photo...");
		mProgress.show(); 
		mProgress.setCanceledOnTouchOutside(false);
		
		new Thread() {
			@Override
			public void run() {
				int what 		= 0;
				
				try {
					mRest.uploadPhoto(provider, dokumen, siteid, var, caption);
				} catch (Exception e) {
					what = 1; 
					e.printStackTrace();
				}
				
				mHandler.sendMessage(mHandler.obtainMessage(what, var));
			}
		}.start();
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			final String var 	= (String) msg.obj;
			
			if(msg.what == 0){
				final ImageUploader imageUploader = new ImageUploader();
				
				imageUploader.setListener(new ImageUploadListener() {
					
					@Override
					public void onStart() {
						Debug.i(Cons.TAG, "Start");
					}
					
					@Override
					public void onProgress(int progress) {
						Debug.i(Cons.TAG, "Progress " + String.valueOf(progress));
					}
					
					@Override
					public void onFinish() {
						Debug.i(Cons.TAG, "Finishh");
						mProgress.dismiss();
						showDialogMessage("Success", "Photo berhasil di upload");
					}
					
					@Override
					public void onError(int errorCode) {
						 Debug.i(Cons.TAG, "Error " + String.valueOf(errorCode));
						showDialogMessage("OOPS", "Gagal upload photo");
					}
				});
				
				imageUploader.upload("tracking_foto_" + var, "tracking_foto_" + var, path, 
						Cons.API_URL + String.format("/save?sitecode=%s&dokumen=%s&provider=%s", siteid, dokumen, provider).replace(" ", "%20"));
			}else{
				mProgress.dismiss();
				showDialogMessage("OOPS", "Gagal upload photo");
			}
		};
	};
}

