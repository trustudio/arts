package com.trustudio.arts.v2.model;

public class ConfigModel {

	public static final String PROVIDER = "PROVIDER";
	public static final String DOKUMEN = "DOKUMEN";
	public static final String SITE_CODE = "SITE_CODE";
	public static final String JOB = "JOB";

	private String provider, dokumen, siteCode, job;

	public ConfigModel() {
	}

	public ConfigModel(String provider, String siteCode, String dokumen, String pekerjaan) {

		this.provider = provider;
		this.siteCode = siteCode;
		this.dokumen = dokumen;
		this.job = pekerjaan;

	}
	


	public ConfigModel(String provider, String siteCode, String dokumen) {

		this.provider = provider;
		this.siteCode = siteCode;
		this.dokumen = dokumen;

	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getDokumen() {
		return dokumen;
	}

	public void setDokumen(String dokumen) {
		this.dokumen = dokumen;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

}
