package com.trustudio.arts.v2.model;

public class TemplateModel {
	
	public static final String TEMPLATE_ID =  "template_dokumen_id";
	public static final String TEMPLATE_ID1 =  "TemplateDokumenId";
	public static final String TEMPALTE_DATA = "data";

	public int Id = 0;
	public int ProviderId = 0;
	public String ProviderId_toString = "";
	public int JenisDokumenId = 0;
	public String JenisDokumenId_toString = "";
	public int JenisPekerjaanId = 0;
	public String JenisPekerjaanId_toString = "";
	public int Version = 0;
	public boolean IsEnabled = true;
	public String Name = "";

}
