package com.trustudio.arts.v2.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Parent database class.
 * 
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 *
 */
public class Database {
	protected SQLiteDatabase mSqLite;
	
	public Database(SQLiteDatabase sqlite) {
		mSqLite = sqlite;
	}
	
	public void reload(SQLiteDatabase sqlite) {
		mSqLite = sqlite;
	}
}