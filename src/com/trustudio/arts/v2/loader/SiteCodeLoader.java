package com.trustudio.arts.v2.loader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trustudio.arts.v2.http.AsyncHttpResponseHandler;
import com.trustudio.arts.v2.http.NexwaveClient;
import com.trustudio.arts.v2.model.SiteCode;
import com.trustudio.arts.v2.util.OpenData;
import com.trustudio.arts.v2.util.URLCons;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

public class SiteCodeLoader {

	private static final String TAG = "#SiteCodeLoader_";
	private Context mContext;

	public SiteCodeLoader(Context context) {
		this.mContext = context;
	}

	public void loadData(final int providerID, final OnSiteCodeLoaderListener listener) {

		String content = OpenData.getSiteCodeData((ContextWrapper) mContext, String.valueOf(providerID));

		if (content != null) {
			initJSON(content, listener);
			return;
		}

		String url = String.format(URLCons.SITE_BY_PROVIDER_ID, providerID);

		NexwaveClient.get(url, null, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				Log.d(getClass().getSimpleName(), content);
				if (initJSON(content, listener))
					OpenData.saveSiteCodeData((ContextWrapper) mContext, String.valueOf(providerID), content);
			}

			@Override
			public void onFailure(Throwable error) {
				error.printStackTrace();
				listener.onError();
			}
		});

	}

	private boolean initJSON(String content, final OnSiteCodeLoaderListener listener) {

		try {
			JSONObject json = new JSONObject(content);

			if (json.getString(NexwaveClient.STATUS).equals(NexwaveClient.STATUS_OK)) {

				JSONArray jarray = json.getJSONArray(SiteCode._DATA);
				String[] data = new String[jarray.length()];

				for (int i = 0; i < jarray.length(); i++) {

					json = jarray.getJSONObject(i);
					data[i] = json.getString(SiteCode.SITE_CODE);

				}

				if (listener != null) {
					listener.onComplete(data);
				}

				return true;
			} else {
				listener.onError();
			}

		} catch (JSONException e) {
			Log.e(TAG, e.toString());
			e.printStackTrace();
		}
		return false;
	}

	/* ======================== Call Back =========================== */

	public abstract static class OnSiteCodeLoaderListener {
		public abstract void onComplete(String[] data);

		public void onError() {
		};
	}

}
