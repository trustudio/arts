package com.trustudio.arts.v2.loader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.util.Log;

import com.trustudio.arts.v2.http.AsyncHttpResponseHandler;
import com.trustudio.arts.v2.http.NexwaveClient;
import com.trustudio.arts.v2.model.TemplateModel;
import com.trustudio.arts.v2.ui.CandidateLocationActivity;
import com.trustudio.arts.v2.ui.SiteListActivity;
import com.trustudio.arts.v2.util.OpenData;
import com.trustudio.arts.v2.util.URLCons;

public class TemplateLoader {

	private static final String TAG = "#TemplateLoader";

	private AlertDialog errorDialog;
	private SiteListActivity  mContext;

	private TemplateModel mModel;

	private OnTemplateLoaderListener listener;

	public TemplateLoader(SiteListActivity candidateLocationActivity) {
		this.mContext = candidateLocationActivity;
	}

	public void loadData(String iden, String sitecode, String provider, String dokumen, String templateData, final OnTemplateLoaderListener listener) {
		this.listener = listener;
		
		/* get template document id */
		boolean isNotError = initData(templateData, null);
		if (!isNotError)
			return;
		
		OpenData.saveTemplateID(mContext, mModel.Id, iden);
		
		boolean isQuestionAvaible = !OpenData.isQuestionDataEmpty(mContext, mModel.Id);
		if (isQuestionAvaible) {
			listener.onLoadComplete(mModel.Id);
			return;
		}

		/* if question not avilable */
		final ProgressDialog pg = getDownloadDialog();
		downloadQuestion(pg, provider, dokumen, sitecode);

	}

	private void downloadQuestion(final ProgressDialog pg, String provider, String dokumen, String sitecode) {

		if (!pg.isShowing())
			pg.show();

		final int templateDocId = mModel.Id;
		
		/* tambahan sigue */
		OpenData.saveSiteCodeData(mContext, provider, dokumen, sitecode);

		if (OpenData.isQuestionDataEmpty(mContext, templateDocId)) {

			String link = String.format(URLCons.GET_TEMPLATE, sitecode, dokumen, provider)
					.replace(" ", "");
			
			NexwaveClient.post(mContext, link, null,
					new AsyncHttpResponseHandler() {
						@Override
						public void onSuccess(String content) {

							Log.d("downloadQuestion", content);

							Log.d("downloaded", content);
							OpenData.saveQuestionData(mContext.getApplication(), content, templateDocId);
							listener.onLoadComplete(mModel.Id);
						}

						@Override
						public void onFailure(Throwable error) {
							NexwaveClient.cancel(mContext);
							Log.e(TAG, error.toString());
							// TODO show error dialog
						}

						@Override
						public void onFinish() {
							pg.dismiss();
						}
					});
		} else {
			pg.dismiss();
		}

	}

	private boolean initData(String content, final ProgressDialog pg) {
		try {
			Log.i("Cek Content1", content);
			
			//JSONObject json = new JSONObject(content).getJSONObject("data");
			//JSONArray jArr = new JSONArray(content).getJSONObject("data");


			JSONObject jsonObj 		= (JSONObject) new JSONTokener(content).nextValue(); 
			JSONArray jsonArray		= jsonObj.getJSONArray("data");
			
			Log.i("Cek Content json", jsonArray.toString());
			
			mModel = new TemplateModel();
			mModel.Name = "TRI - TESTING - Ver. 1"; /*json.getString("provider") + "-"
					+ json.getString("dokumen") + "-" + json.getString("sitecode");
			
			/* json.getString("provider") + "-" + json.getString("pekerjaan") + "-"
					+ json.getString("dokumen") + "-" + json.getString("sitecode"); */
			
			mModel.Id = jsonArray.getJSONObject(0).getInt(TemplateModel.TEMPLATE_ID1);
			
			Log.i("Cek Content2", content);
			
			OpenData.saveTemplate(mContext, content, mModel.Id);

			return true;

		} catch (Exception e) {

			if (pg != null)
				pg.dismiss();

			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setPositiveButton("OK", null);
			builder.setMessage("Data empty");
			builder.setTitle("Oops!");

			if (errorDialog == null)
				errorDialog = builder.create();
			if (!errorDialog.isShowing())
				errorDialog.show();

			Log.e("onLoadData", e.toString());

			return false;
		}
	}

	/* ================ Helper ================ */

	private ProgressDialog getDownloadDialog() {
		final ProgressDialog pg = new ProgressDialog(mContext);
		pg.setTitle("Downloading...");
		pg.setMessage("Downloading Question...");
		return pg;
	}

	/* ================ Interface ============= */

	public interface OnTemplateLoaderListener {
		void onLoadComplete(int templateDocID);
	}

}
